# encoding: utf-8

require 'simplecov'
SimpleCov.start

require 'jekyll'
require 'byebug'
require File.expand_path('../lib/jekyll-category_generator.rb', File.dirname(__FILE__))

Jekyll.logger.log_level = :error

RSpec.configure do |config|
  config.run_all_when_everything_filtered = true
  config.filter_run focus: true
  config.order = 'random'

  SOURCE_DIR = File.expand_path('../fixtures', __FILE__)
  DEST_DIR   = File.expand_path('../site',     __FILE__)
  CATEGORY_INDEX_PATH = '/_layouts/category_index.html'.freeze

  ##
  # Get a source file or directory
  def source_dir(*files)
    File.join(SOURCE_DIR, *files)
  end

  ##
  # Get a destination file or directory
  def dest_dir(*files)
    File.join(DEST_DIR, *files)
  end

  ##
  # Create the category_index layout
  def create_category_index_layout
    FileUtils.cp("#{source_dir}/_templates/category_index.html", "#{source_dir}#{CATEGORY_INDEX_PATH}")
  end

  ##
  # Creates a user _config file
  def specify_user_config_file
    FileUtils.cp("#{source_dir}/_templates/user_config.yml", "#{source_dir}/_config.yml")
  end

  ##
  # Clear the destination directory.
  def clear_dest
    FileUtils.rm_rf(dest_dir)
    FileUtils.rm_rf(source_dir('.jekyll-metadata'))
  end

  ##
  # Remove the source files
  def clear_source
    category_index_file = "#{source_dir}#{CATEGORY_INDEX_PATH}"
    FileUtils.rm(category_index_file) if File.exist?(category_index_file)
    File.new("#{source_dir}/_config.yml", 'w')
    FileUtils.rm_rf(source_dir('.jekyll-metadata'))
  end
end
