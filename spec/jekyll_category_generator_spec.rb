# encoding: UTF-8

require 'spec_helper'

describe(Jekyll::CategoryGenerator) do
  let(:overrides) do
    {
      'source' => source_dir,
      'destination' => dest_dir,
      'url' => 'http://example.org'
    }
  end
  let(:config) do
    Jekyll.configuration(overrides)
  end
  let(:site) { Jekyll::Site.new(config) }

  before(:each) do
    clear_source
    # clear_dest
  end

  context 'Without the category_index layout,' do
    it 'should raise an error message' do
      expect { site.process }.to raise_error RuntimeError
    end
  end

  context 'With the category_index layout,' do
    context 'When using the default configurations,' do
      before(:each) do
        create_category_index_layout
        site.process
        @file = File.read("#{dest_dir}/#{Jekyll::CategoryGenerator::CATEGORIES_FOLDER}/Heroku/index.html")
      end

      it 'should create 2 category folders' do
        dir = Dir.entries("#{dest_dir}/#{Jekyll::CategoryGenerator::CATEGORIES_FOLDER}")
        expect(dir).to contain_exactly('.', '..', 'Heroku', 'Ruby on Rails')
      end

      it 'date_to_html_string filter should format the given date' do
        expect(@file).to match('2016-12-31')
        expect(@file).to match('Category: Heroku')
      end

      it 'category_links filter should create a blank separated list of link of the given categories' do
        categories = Jekyll::CategoryGenerator::CATEGORIES_FOLDER
        file = File.read("#{dest_dir}/heroku/ruby\ on\ rails/2016/12/31/post-with-2-categories.html")
        expect(file).to match("<a class='category' href='/#{categories}/Heroku/'>Heroku</a> <a")
        expect(file).to match("</a> <a class='category' href='/#{categories}/Ruby%20on%20Rails/'>Ruby on Rails</a>")
      end
    end

    context 'When overriting default configurations,' do
      before(:each) do
        create_category_index_layout
        specify_user_config_file
        site.process
      end

      it 'should have changed the directories folder' do
        expect(File.file?("#{dest_dir}/#{Jekyll::CategoryGenerator::CATEGORIES_FOLDER}/Heroku/index.html")).to be false
      end

      it 'date_to_html_string filter should format the given date in french' do
        file = File.read("#{dest_dir}/my_categories/Heroku/index.html")
        expect(file).to match('31 décembre 2016')
        expect(file).to match('Catégorie : Heroku')
      end

      it 'category_links filter should create a comma separated list of link of the given categories' do
        file = File.read("#{dest_dir}/heroku/ruby\ on\ rails/2016/12/31/post-with-2-categories.html")
        expect(file).to match("<a class='category' href='/my_categories/Heroku/'>Heroku</a>, <a")
        expect(file).to match("</a>, <a class='category' href='/my_categories/Ruby%20on%20Rails/'>Ruby on Rails</a>")
      end
    end
  end

  context 'When unit testing,' do
    before(:each) do
      @context = Liquid::Context.new({}, {}, site: site)
    end

    it '#1 Category should handle an empty/nil name' do
      list = Jekyll::Categories.new(nil, @context)
      expect(list.links).to eq ''
    end

    it 'Category should render a link' do
      category = Jekyll::Category.new('test', @context)
      expect(category.link).to eq "<a class='category' href='/categories/test/'>test</a>"
    end

    it '#3 Site should handle an empty/nil category_dir configuration' do
      site.config['jekyll-category_generator'] = {}
      expect(site.categories_folder).to eq 'categories'
    end
  end
end
