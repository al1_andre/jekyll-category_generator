# encoding: utf-8

# Jekyll category page generator.
# Copyright (c) 2010 Dave Perrett, http://recursive-design.com/
# Copyright (c) 2017 Alain ANDRE, http://alain-andre.fr/
# Licensed under the MIT license (http://www.opensource.org/licenses/mit-license.php)

require 'jekyll/category_generator/version'
require 'jekyll/category_generator/site_override.rb'
require 'jekyll/category_generator/category_filters.rb'

# Includes the plugin into the Jekyll module
module Jekyll
  # The category generator module
  module CategoryGenerator
    CATEGORIES_FOLDER = 'categories'.freeze
    # Jekyll hook - the generate method is called by jekyll, and generates all of the category pages.
    class CGenerator < Generator
      safe true
      priority :low
      def generate(site)
        site.write_category_indexes
      end
    end
  end
end

Liquid::Template.register_filter(Jekyll::CategoryFilters)
