# encoding: utf-8

require 'jekyll/category_generator/category.rb'

# Includes the plugin into the Jekyll module
module Jekyll
  ##
  # Managing many Category
  class Categories
    ##
    # Initialize the list of categories by passing a list of String.
    #
    # +category_names+ An array of strings representing a list of categories.
    # +context+ is the Liquid:Tag context
    #
    def initialize(category_names, context)
      @context = context
      category_names = [category_names] unless category_names.is_a? Array
      initialize_category_list(category_names, context)
    end

    ##
    # Initialize the category list
    #
    # +category_names+ An array of strings representing a list of categories.
    # +context+ is the Liquid:Tag context
    #
    def initialize_category_list(category_names, context)
      @category_list = category_names.map { |string| Category.new(string, context) }
    end

    ##
    # Outputs a list of categories as separated <a> links. This is used
    # to output the category list for each post on a category page.
    #
    # Returns string
    #
    def links
      separate(@category_list.sort!.map(&:link))
    end

    private

    ##
    # Join the list of category
    def separate(list_of_links)
      list_of_links.join(category_link_separator).to_s
    end

    ##
    # Get the defined category link separator
    def category_link_separator
      @context.registers[:site].config['jekyll-category_generator']['link_separator']
    rescue
      ' '
    end
  end
end
