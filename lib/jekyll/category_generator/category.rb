# encoding: utf-8

require 'uri'

# Includes the plugin into the Jekyll module
module Jekyll
  ##
  # Managing a Category
  class Category
    include Comparable

    attr_reader :name

    ##
    # Initialize the Category
    #
    # +name+ is the name of the current Category
    # +context+ is the Liquid:Tag context
    #
    def initialize(name, context)
      @name = name
      @context = context
    end

    ##
    # Compare this Category against another Category.
    # Comparison is a comparison between the 2 names.
    #
    def <=>(other)
      return nil unless name.is_a? String
      name <=> other.name
    end

    ##
    # Outputs a single category as an <a> link using the given category_dir.
    #
    # Returns html formatted string
    #
    def link
      return '' until @name
      "<a class='category' href='/#{categories_folder}/#{URI.encode(@name)}/'>#{@name}</a>"
    end

    ##
    # Returns the categories folder
    #
    def categories_folder
      @context.registers[:site].config['jekyll-category_generator']['category_dir']
    rescue
      CategoryGenerator::CATEGORIES_FOLDER
    end
  end
end
