# encoding: utf-8

# Includes the plugin into the Jekyll module
module Jekyll
  # The CategoryIndex class creates a single category page for the specified category.
  class CategoryIndex < Page
    # Initializes a new CategoryIndex.
    #
    #  +base+         is the String path to the <source>.
    #  +category_dir+ is the String path between <source> and the category folder.
    #  +category+     is the category currently being processed.
    def initialize(site, base, category_dir, category)
      @site = site
      @base = base
      @dir = category_dir
      @name = 'index.html'
      process(@name)
      # Read the YAML data from the layout page.
      read_yaml(File.join(base, '_layouts'), 'category_index.html')
      data['category'] = category
      # Set the title for this page.
      data['title'] = "#{title_prefix}#{category}"
    end

    ##
    # Return the category title prefix
    def title_prefix
      site.config['jekyll-category_generator']['title_prefix']
    rescue
      'Category: '
    end
  end
end
