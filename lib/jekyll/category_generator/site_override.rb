# encoding: utf-8

require 'jekyll/category_generator/category.rb'
require 'jekyll/category_generator/categories.rb'
require 'jekyll/category_generator/category_index.rb'

# Includes the plugin into the Jekyll module
module Jekyll
  # The Site class is a built-in Jekyll class with access to global site config information.
  class Site
    # Creates an instance of CategoryIndex for each category page, renders it, and
    # writes the output to a file.
    #
    #  +category_dir+ is the String path to the category folder.
    #  +category+     is the category currently being processed.
    def write_category_index(category_dir, category)
      index = CategoryIndex.new(self, source, category_dir, category)
      index.render(layouts, site_payload)
      index.write(dest)
      # Record the fact that this page has been added, otherwise Site::cleanup will remove it.
      pages << index
    end

    # Loops through the list of category pages and processes each one.
    def write_category_indexes
      raise_unexisting_layout_error unless layouts.key? 'category_index'
      categories.keys.each do |category|
        write_category_index(File.join(categories_folder, category), category)
      end
    end

    ##
    # Returns the categories folder
    #
    def categories_folder
      category_dir = config['jekyll-category_generator']['category_dir']
      return CategoryGenerator::CATEGORIES_FOLDER unless category_dir
      category_dir
    rescue
      CategoryGenerator::CATEGORIES_FOLDER
    end

    # Raise an error on unexisting layout
    def raise_unexisting_layout_error
      raise <<-ERR

  =======================================================================
   Error for jekyll-category_generator.rb plugin
  -----------------------------------------------------------------------
   No 'category_index.html' in source/_layouts/ !!
   You should create it first. See the plugin home page for more details
  -----------------------------------------------------------------------
   Home page : https://gitlab.com/al1_andre/jekyll-category-generator
  =======================================================================

ERR
    end
  end
end
