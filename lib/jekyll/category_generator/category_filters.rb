# encoding: utf-8

require 'jekyll/category_generator/time_override.rb'
require 'jekyll/category_generator/categories.rb'

# Includes the Filters into the Jekyll module
module Jekyll
  # Adds some extra filters used during the category creation process.
  module CategoryFilters
    ##
    # Outputs a list of categories as comma-separated <a> links. This is used
    # to output the category list for each post on a category page.
    #
    # Returns string
    def category_links(categories)
      Categories.new(categories, @context).links
    end

    ##
    # Outputs a single category as an <a> link.
    #
    # Returns string
    def category_link(category)
      Category.new(category, @context).link
    end

    ##
    # Outputs the post.date as formatted html, with hooks for CSS styling.
    #
    #  +date+ is the date object to format as HTML.
    #
    # Returns string
    def date_to_html_string(date)
      date.strftime_with(format, months)
    end

    ##
    # Return the configured format
    #
    # Returns string
    def format
      @context.registers[:site].config['jekyll-category_generator']['date']['format']
    rescue
      '%Y-%m-%d'
    end

    ##
    # Return the configured months list
    #
    # Returns string
    def months
      @context.registers[:site].config['jekyll-category_generator']['date']['months']
    rescue
      nil
    end
  end
end
