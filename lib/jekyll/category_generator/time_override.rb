require 'date'
require 'time'

##
# Overriding Time class
class Time
  def strftime_with(format, months)
    if months
      months = [nil] + months.split(' ')
      format = format.dup
      format.gsub!(/%B/, months[mon])
    end
    strftime(format)
  end
end
